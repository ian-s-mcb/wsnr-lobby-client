import { useState } from 'react'

import { COMMAND_NICK } from '../constants'
import './NicknameForm.css'

const NicknameForm = ({ members, ws }) => {
  // State
  const [feedback, setFeedback] = useState('')
  const [value, setValue] = useState('')
  const [valid, setValid] = useState('')

  // Callbacks
  function handleChange (e) {
    // Update value
    const val = e.target.value
    setValue(val)

    // Query input element to get validity info
    const el = document.getElementById('nicknameInput')

    // Check if nickname is taken
    const isTaken = members
      .map(m => m.nickname)
      .includes(val)

    // Apply validility result
    if (isTaken) {
      setFeedback('That nickname is taken.')
      setValid('is-invalid')
    } else if (!el.validity.valid) {
      setFeedback(el.validationMessage)
      setValid('is-invalid')
    } else {
      setValid('is-valid')
    }
  }
  function onSubmit (e) {
    e.preventDefault()
    if (valid === 'is-valid') {
      ws.send(`${COMMAND_NICK} ${value}`)
    }
  }

  return (
    <form
      className='mt-5 p-4 rounded-3'
      id='nicknameForm'
      onSubmit={onSubmit}
    >
      <div className='fs-2 text-center'>wsnr-lobby</div>
      <label htmlFor='nicknameInput' className='form-label'>
        Nickname
      </label>
      <input
        autoFocus='autofocus'
        className={`form-control ${valid}`}
        id='nicknameInput'
        minLength='3'
        onChange={handleChange}
        pattern='[\w-]*'
        required
        type='text'
        value={value}
      />
      <div className='valid-feedback'>
        Looks good!
      </div>
      <div className='invalid-feedback'>
        {feedback}
      </div>
      <button className='btn btn-primary mt-3'>Enter lobby</button>
    </form>
  )
}

export default NicknameForm
