const Footer = () => (
  <footer
    className='d-flex justify-content-between mt-5 text-secondary'
  >
    <div>
      @ 2021&nbsp;
      <a
        href='https://gitlab.com/ian-s-mcb'
      >
        ian-s-mcb
      </a>.
    </div>
    <div>
      Source:&nbsp;
      <a
        href='https://gitlab.com/ian-s-mcb/wsnr-lobby-server'
      >
        server
      </a>
      ,&nbsp;
      <a
        href='https://gitlab.com/ian-s-mcb/wsnr-lobby-client'
      >
        client
      </a>.
    </div>
  </footer>
)

export default Footer
