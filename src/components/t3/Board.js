import Square from './Square'

// Helper object
const winningCellIndex2LineCoord = {
  '012': { x1: 0.2, y1: 0.5, x2: 2.8, y2: 0.5 },
  345: { x1: 0.2, y1: 1.5, x2: 2.8, y2: 1.5 },
  678: { x1: 0.2, y1: 2.5, x2: 2.8, y2: 2.5 },
  '036': { x1: 0.5, y1: 0.2, x2: 0.5, y2: 2.8 },
  147: { x1: 1.5, y1: 0.2, x2: 1.5, y2: 2.8 },
  258: { x1: 2.5, y1: 0.2, x2: 2.5, y2: 2.8 },
  '048': { x1: 0.2, y1: 0.2, x2: 2.8, y2: 2.8 },
  246: { x1: 2.8, y1: 0.2, x2: 0.2, y2: 2.8 }
}

// Helper component
const WinningLineSVG = ({ winningLine }) => (
  <svg id='t3WinningLine' viewBox='0 0 3 3'>
    <line
      stroke='#ff0000'
      strokeWidth='8'
      vectorEffect='non-scaling-stroke'
      {...winningCellIndex2LineCoord[winningLine]}
    />
  </svg>
)

const Board = ({ gameState, onSquareClick, winningLine }) => (
  <div id='t3BoardWrapper'>
    <div id='t3Board'>
      {gameState.map((value, i) => (
        <Square key={i} onClick={() => onSquareClick(i)} value={value} />
      ))}
    </div>
    {winningLine ? <WinningLineSVG winningLine={winningLine} /> : null}
  </div>
)

export default Board
