const TurnCountdown = ({ isYourTurn, tie, timerRemainingTime, winner }) => (
  (winner || tie || !isYourTurn)
    ? null
    : (
      <div className={(timerRemainingTime < 6) ? 'text-danger' : ''}>
        {`${timerRemainingTime} seconds`}
        <br />
        remaining
      </div>
      )
)

export default TurnCountdown
