import { getOpponent } from '../../utils'

function TurnDisplay ({ isYourTurn, nickname, players, tie, winner }) {
  let gameOverMessage
  let gameOverMessageClass

  const opponent = getOpponent(nickname, players)

  // Game over display elements
  if (winner === nickname) {
    gameOverMessage = 'You Won'
    gameOverMessageClass = 'fs-3 text-success'
  } else if (winner === opponent) {
    gameOverMessage = 'You Lost'
    gameOverMessageClass = 'fs-3 text-danger'
  } else {
    gameOverMessage = 'You tied'
    gameOverMessageClass = 'fs-3'
  }

  return (
    <div className='d-grid text-center'>
      {winner || tie
        ? (
          <>
            <span>Game Over</span>
            <br />
            <span className={gameOverMessageClass}>
              {gameOverMessage}
            </span>
          </>
          )
        : (
            isYourTurn
              ? <span className='text-info'>Your Turn</span>
              : <span>{`${opponent}'s turn`}</span>
          )}
    </div>
  )
}

export default TurnDisplay
