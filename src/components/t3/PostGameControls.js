// Helper components
const ApproveButton = ({ approveRematch }) => (
  <button
    className='btn btn-outline-primary btn-sm'
    onClick={approveRematch}
  >
    Approve rematch
  </button>
)
const ExitButton = ({ exitGame }) => (
  <button className='btn btn-primary btn-sm' onClick={exitGame}>
    Re-enter lobby
  </button>
)
const LonePlayerButton = () => (
  <button
    className='btn btn-outline-primary btn-sm'
    disabled
  >
    Opponent exited
  </button>
)
const RequestButton = ({ requestRematch }) => (
  <button
    className='btn btn-outline-primary btn-sm'
    onClick={requestRematch}
  >
    Request rematch
  </button>
)

const RequestRematchButton = ({
  approveRematch, lonePlayer, nickname, rematchRequested,
  rematchRequester, requestRematch
}) => (
  lonePlayer
    ? <LonePlayerButton />
    : rematchRequested && (rematchRequester === nickname)
      ? <span>Waiting for answer</span>
      : rematchRequested
        ? <ApproveButton approveRematch={approveRematch} />
        : <RequestButton requestRematch={requestRematch} />
)

const PostGameControls = ({
  approveRematch, exitGame, lonePlayer, nickname, rematchRequested,
  rematchRequester, requestRematch, setEngaged, tie, winner, ws
}) => (
  <div
    id='t3PostGameControls'
    className='d-flex justify-content-evenly my-3'
  >
    {
      winner || tie
        ? (
          <>
            <ExitButton exitGame={exitGame} />
            <RequestRematchButton
              approveRematch={approveRematch}
              lonePlayer={lonePlayer}
              nickname={nickname}
              rematchRequested={rematchRequested}
              rematchRequester={rematchRequester}
              requestRematch={requestRematch}
            />
          </>
          )
        : null
          }
  </div>
)

export default PostGameControls
