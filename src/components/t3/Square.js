const classes = `
  d-flex
  display-1
  align-items-center
  justify-content-center
  m-3
`

const Square = ({ value, onClick }) => (
  value
    ? (
      <div className={classes}>
        {value === 'X'
          ? <i className='bi bi-x-lg display-1' />
          : <i className='bi bi-circle display-1' />}
      </div>)
    : (
      <button
        className='btn btn-outline-primary m-3'
        onClick={onClick}
      >
        {value}
      </button>)
)

export default Square
