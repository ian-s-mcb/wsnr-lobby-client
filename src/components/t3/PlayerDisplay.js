const PlayerDisplay = ({ isMe, name, symbol }) => (
  <div className='t3-player-display'>
    {symbol === 'X'
      ? <i className='bi bi-x-lg fs-1' />
      : <i className='bi bi-circle fs-1' />}
    <div style={{ marginTop: '-10px' }}>
      {
        isMe
          ? (
            <div className='text-info'>
              <span>{name}</span>
              <br />
              <div style={{ marginTop: '-7px' }}>(you)</div>
            </div>
            )
          : <div>{name}</div>
      }
    </div>
  </div>
)

export default PlayerDisplay
