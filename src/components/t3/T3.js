import { useEffect, useState } from 'react'

import Board from './Board'
import {
  COMMAND_APPROVE_REMATCH,
  COMMAND_TURN,
  COMMAND_FORFEIT,
  COMMAND_EXIT,
  COMMAND_REQ_REMATCH
} from '../../constants'
import { getSymbol, checkIfYourTurn } from '../../utils'
import { useInterval } from '../../customHooks'
import './T3.css'
import PlayerDisplay from './PlayerDisplay'
import PostGameControls from './PostGameControls'
import TurnCountdown from './TurnCountdown'
import TurnDisplay from './TurnDisplay'

// Constants
const TIMER_DELAY = 1000
const TIMER_DURATION = 10

function T3 ({
  gameState, lonePlayer, nickname, players, player1Turn,
  rematchRequested, rematchRequester, setEngaged, setGameState, tie,
  winner, winningLine, ws
}) {
  // State
  const [isYourTurn, setIsYourTurn] = useState(false)
  const [isTimerRunning, setIsTimerRunning] = useState(true)
  const [timerRemainingTime, setTimerRemainingTime] = useState(
    TIMER_DURATION
  )

  // Effect hook
  // Update state if turn changes
  useEffect(() => {
    const bool = checkIfYourTurn(nickname, players, player1Turn)
    setIsYourTurn(bool)
    setTimerRemainingTime(TIMER_DURATION)
    setIsTimerRunning(bool)
  }, [nickname, players, player1Turn])

  // Timer hook
  useInterval(() => {
    setTimerRemainingTime(timerRemainingTime - 1)
    if (timerRemainingTime < 2) { forefitGame() }
  }, (
    (!winner && !tie && isTimerRunning && isYourTurn)
      ? TIMER_DELAY
      : null
  ))

  // Helpers
  function approveRematch () {
    // Ignore if game hasn't finished
    if (!winner) { return }

    ws.send(`${COMMAND_APPROVE_REMATCH}`)
  }

  function forefitGame () {
    setIsTimerRunning(false)
    setTimerRemainingTime(TIMER_DURATION)
    ws.send(`${COMMAND_FORFEIT}`)
  }

  function onSquareClick (i) {
    // Ignore click if not player's turn
    if (!isYourTurn) { return }

    // Ignore click if game over
    if (winner) { return }

    if (gameState[i] === null) {
      const newState = [...gameState]
      // Place symbol in game state
      newState[i] = getSymbol(nickname, players)
      setGameState(newState)
      const newStateStr = JSON.stringify(newState)

      ws.send(`${COMMAND_TURN} ${newStateStr}`)
    }
  }
  function exitGame () {
    ws.send(`${COMMAND_EXIT}`)
  }

  function requestRematch () {
    ws.send(`${COMMAND_REQ_REMATCH}`)
  }

  return (
    <div className='mt-3' id='t3'>
      <div className='d-flex justify-content-evenly mt-3 text-center'>
        <PlayerDisplay
          isMe={nickname === players.player1}
          name={players.player1}
          symbol='X'
        />
        <div id='t3TurnContainer'>
          <TurnDisplay
            isYourTurn={isYourTurn}
            nickname={nickname}
            players={players}
            tie={tie}
            winner={winner}
          />
          <TurnCountdown
            isYourTurn={isYourTurn}
            timerRemainingTime={timerRemainingTime}
            winner={winner}
            tie={tie}
          />
        </div>
        <PlayerDisplay
          isMe={nickname === players.player2}
          name={players.player2}
          symbol='O'
        />
      </div>
      <PostGameControls
        approveRematch={approveRematch}
        lonePlayer={lonePlayer}
        nickname={nickname}
        exitGame={exitGame}
        rematchRequested={rematchRequested}
        rematchRequester={rematchRequester}
        requestRematch={requestRematch}
        setEngaged={setEngaged}
        tie={tie}
        winner={winner}
        ws={ws}
      />
      <Board
        onSquareClick={onSquareClick}
        gameState={gameState}
        winningLine={winningLine}
      />
    </div>
  )
}

export default T3
