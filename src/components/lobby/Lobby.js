import { useState } from 'react'

import {
  COMMAND_STATUS,
  STATUS_IDLE,
  STATUS_READY
} from '../../constants'
import StatusButton from './StatusButton'
import MemberList from './MemberList'
import { useInterval } from '../../customHooks'

const TIMER_DELAY = 1000
const TIMER_DURATION = 120

const Lobby = ({ members, nickname, ws }) => {
  // State
  const [isTimerRunning, setIsTimerRunning] = useState(true)
  const [timerElapsedTime, setTimerElapsedTime] = useState(0)

  // Timer hook
  useInterval(
    () => {
      setTimerElapsedTime(timerElapsedTime + 1)
      if (timerElapsedTime >= TIMER_DURATION) { stopTimer(true) }
    },
    isTimerRunning ? TIMER_DELAY : null
  )

  // Helper functions
  function startTimer () {
    setIsTimerRunning(true)
    setTimerElapsedTime(0)
    ws.send(`${COMMAND_STATUS} ${STATUS_READY}`)
  }
  function stopTimer (sendMessage) {
    setIsTimerRunning(false)
    setTimerElapsedTime(0)
    if (sendMessage) { ws.send(`${COMMAND_STATUS} ${STATUS_IDLE}`) }
  }
  function toggleTimer () {
    if (isTimerRunning) { stopTimer(true) } else { startTimer() }
  }

  return (
    <div className='mt-2'>
      <h2>
        <span className='badge bg-secondary rounded-pill'>{members.length}</span> Users in the lobby
      </h2>
      <p>
        Users can be either <code>idle</code>, <code>ready</code>, or <code>busy</code>.
      </p>
      <p>
        Click on a <span className='p-1 list-group-item-danger'>ready user in red</span> to engage.
      </p>
      <StatusButton
        isTimerRunning={isTimerRunning}
        timerElapsedTime={timerElapsedTime}
        toggleTimer={toggleTimer}
      />
      <MemberList
        nickname={nickname}
        members={members}
        stopTimer={stopTimer}
        ws={ws}
      />
    </div>
  )
}

export default Lobby
