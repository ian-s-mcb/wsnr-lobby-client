function StatusButton ({
  isTimerRunning, timerElapsedTime, toggleTimer
}) {
  const textWhenTimerIsRunning = (
    <span>
      {`You've been waiting ${timerElapsedTime} seconds`}
      <br />
      Click to cancel wait
    </span>
  )

  return (
    <div className='d-grid'>
      <button
        className='btn btn-outline-primary'
        id='waitButton'
        onClick={toggleTimer}
      >
        {
          isTimerRunning
            ? textWhenTimerIsRunning
            : <span>Mark as ready</span>
        }
      </button>
    </div>
  )
}

export default StatusButton
