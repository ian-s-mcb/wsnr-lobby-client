import {
  COMMAND_ENGAGE,
  STATUS_BUSY,
  STATUS_READY
} from '../../constants'
import MemberListItem from './MemberListItem'

// Constants
// Codes are member status in the order that they appear in the
// memberlist: ready at the top, idle at the bottom
const STATUS_CODE = {
  ready: 0,
  busy: 1,
  idle: 2
}

function MemberList ({ nickname, members, stopTimer, ws }) {
  // Event callback
  function handleUserButtonClick (opponentName) {
    // Validate
    const amIBusy = members.find(
      m => (m.nickname === nickname) && (m.readyStatus === STATUS_BUSY)
    )
    const isOpponentReady = members.find(
      m => (m.nickname === opponentName) && (m.readyStatus === STATUS_READY)
    )

    // Disregard click
    if (amIBusy || !isOpponentReady) { return }

    stopTimer(false)
    ws.send(`${COMMAND_ENGAGE} ${opponentName}`)
  }

  // Helper function
  function sortByStatusThenNickname (a, b) {
    const diff = STATUS_CODE[a.readyStatus] - STATUS_CODE[b.readyStatus]
    if (diff === 0) {
      // Ensure nicknames are compared as strings
      const aNickname = '' + a.nickname
      const bNickname = '' + b.nickname
      if (aNickname === nickname) { return 1 }
      if (bNickname === nickname) { return -1 }
      if (aNickname < bNickname) { return -1 }
      if (aNickname > bNickname) { return 1 }
      return 0
    }
    return diff
  }

  return (
    <div className='list-group mt-3'>
      {
        members
          .sort(sortByStatusThenNickname)
          .map(
            (member) => (
              <MemberListItem
                key={member.nickname}
                handleUserButtonClick={handleUserButtonClick}
                member={member}
                nickname={nickname}
              />
            ))
      }
    </div>
  )
}

export default MemberList
