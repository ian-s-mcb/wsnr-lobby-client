import {
  STATUS_READY
} from '../../constants'

function MemberListItem ({ handleUserButtonClick, member, nickname }) {
  const objIsReady = (
    (member.readyStatus === STATUS_READY) &&
    (member.nickname !== nickname)
  )

  const objIsMe = member.nickname === nickname
  const displayName = `${member.nickname} ${objIsMe ? ' (you)' : ''}`
  const onClick = () => {
    if (objIsReady) {
      handleUserButtonClick(member.nickname)
    }
  }

  return (
    <button
      className={`
        list-group-item
        list-group-item-action
        list-group-item-danger
        ${objIsReady ? '' : 'disabled'}
        ${objIsMe ? 'text-info' : ''}
      `}
      data-bs-toggle='tooltip'
      data-bs-placement='top'
      title='Engage user'
      key={member.nickname}
      onClick={onClick}
    >
      {displayName} - {member.readyStatus}
    </button>
  )
}

export default MemberListItem
