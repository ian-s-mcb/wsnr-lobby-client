import { useState } from 'react'
import { Modal } from 'bootstrap'
import 'bootstrap-icons/font/bootstrap-icons.css'

import './App.css'
import { STATUS_BUSY, STATUS_READY } from './constants'
import DisconnectModal from './components/lobby/DisconnectModal'
import Footer from './components/Footer'
import Lobby from './components/lobby/Lobby'
import NicknameForm from './components/NicknameForm'
import T3 from './components/t3/T3'
import { ws } from './utils'

function App () {
  // State
  const [engaged, setEngaged] = useState(false)
  const [gameState, setGameState] = useState([])
  const [lonePlayer, setLonePlayer] = useState(true)
  const [members, setMembers] = useState([])
  const [nickname, setNickname] = useState('')
  const [players, setPlayers] = useState({ player1: null, player2: null })
  const [player1Turn, setPlayer1Turn] = useState(true)
  const [rematchRequested, setRematchRequested] = useState(false)
  const [rematchRequester, setRematchRequester] = useState(null)
  const [tie, setTie] = useState(false)
  const [winner, setWinner] = useState(null)
  const [winningLine, setWinningLine] = useState(null)

  // Show disconnect model
  ws.onclose = e => {
    console.log('onclose')
    const el = document.getElementById('disconnectModal')
    const modal = new Modal(el)
    modal.show()
    setMembers([])
    setNickname('')
    setEngaged(false)
  }

  // Process message
  ws.onmessage = e => {
    const m = JSON.parse(e.data)
    console.log(m)

    // Update unconditional state
    setMembers([...m.members])
    setLonePlayer(m.lonePlayer)

    // Update conditional state
    // Disregard message if not about recipient or recipient's opponent
    if (!((m.recipient === m.nickname) || (m.recipient === m.opponent))) {
      return
    }
    // Update recipient's nickname
    if (m.recipient === m.nickname) {
      setNickname(m.recipient)
    }
    // Update game if it has started
    if (m.readyStatus === STATUS_BUSY) {
      // Ensure lobby is hidden, game is shown
      setEngaged(true)
      setGameState([...m.gameState])
      setPlayer1Turn(m.player1Turn)
      setPlayers({ ...m.players })
      setWinner(m.winner)
      setWinningLine(m.winningLine)
      setTie(m.tie)

      // Handle rematch request/reply
      setRematchRequested(m.rematchRequested)
      setRematchRequester(m.rematchRequester)
    // Return to lobby if sender exited game and sender is recipient
    } else if (
      (m.readyStatus === STATUS_READY) && (m.recipient === m.nickname)
    ) {
      setEngaged(false)
    }
  }

  // Terminate
  ws.onerror = e => {
    console.log('onerror')
    throw new Error('WebSocket error', e)
  }

  return (
    <div id='app'>
      {engaged
        ? <T3
            gameState={gameState}
            lonePlayer={lonePlayer}
            nickname={nickname}
            player1Turn={player1Turn}
            players={players}
            rematchRequested={rematchRequested}
            rematchRequester={rematchRequester}
            setEngaged={setEngaged}
            setGameState={setGameState}
            tie={tie}
            winner={winner}
            winningLine={winningLine}
            ws={ws}
          />
        : (nickname
            ? <Lobby members={members} nickname={nickname} ws={ws} />
            : <NicknameForm members={members} ws={ws} />)}
      <Footer />
      <DisconnectModal />
    </div>
  )
}

export default App
