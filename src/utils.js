// WebSocket object
const host = process.env.NODE_ENV === 'development'
  ? 'ws://localhost:5000'
  : 'wss://wsnr-lobby-server.herokuapp.com'
const ws = new WebSocket(host)

// T3 helpers
const checkIfYourTurn = (nickname, players, player1Turn) => (
  player1Turn
    ? nickname === players.player1
    : nickname === players.player2
)

const getOpponent = (nickname, players) => (
  nickname === players.player1
    ? players.player2
    : players.player1
)

function getSymbol (nickname, players) {
  let symbol
  if (nickname === players.player1) {
    symbol = 'X'
  } else if (nickname === players.player2) {
    symbol = 'O'
  } else {
    console.log('Invalid nickname/players')
    return
  }

  return symbol
}

export { checkIfYourTurn, getOpponent, getSymbol, ws }
