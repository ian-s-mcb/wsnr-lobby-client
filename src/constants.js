// Message receive types
export const COMMAND_APPROVE_REMATCH = '/approveRematch'
export const COMMAND_ENGAGE = '/engage'
export const COMMAND_FORFEIT = '/forfeit'
export const COMMAND_NICK = '/nick'
export const COMMAND_EXIT = '/exitGame'
export const COMMAND_REQ_REMATCH = '/requestRematch'
export const COMMAND_STATUS = '/status'
export const COMMAND_TURN = '/turn'

// Ready status values
export const STATUS_IDLE = 'idle'
export const STATUS_BUSY = 'busy'
export const STATUS_READY = 'ready'
